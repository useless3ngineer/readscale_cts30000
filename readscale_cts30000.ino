// V1.2
#include <SoftwareSerial.h>
#include <string.h>

const unsigned int GOOD_PIN = 7;
const unsigned int BAD_PIN = 6;
int counter = 0;
int particles = 0;
const unsigned int MAX_MESSAGE_LENGTH = 20;
float general_weight = 0;
float unit_weight = 0;
bool IS_GOOD_SET = false;
bool IS_BAD_SET = false;
float scale_drift = 0;
const int RX_PIN = 10;
const int TX_PIN = 11;

SoftwareSerial scale = SoftwareSerial(RX_PIN, TX_PIN);

void setup()
{
  delay(250);
  Serial.begin(9600);
  pinMode(BAD_PIN, OUTPUT);
  digitalWrite(BAD_PIN, LOW);
  delay(500);
  pinMode(GOOD_PIN, OUTPUT);
  digitalWrite(GOOD_PIN, LOW);

  scale.begin(9600);

    
}

void loop()
{

  if(scale.available() > 0)
  {
       static char message[MAX_MESSAGE_LENGTH];
       static unsigned int message_pos = 0;

      char inByte = scale.read();
      if ( inByte != '\n' && (message_pos < MAX_MESSAGE_LENGTH - 1))
      {
         message[message_pos] = inByte;
         message_pos++;
         
      }   
      else
      {
         //Add null character to string
        message[message_pos] = '\0';
        if(message[0] == 'G')
        {
          char* num = &message[8];
          
          general_weight = atof(num);
        }
        if(message[0] == 'U')
        {
          char* num = &message[7];           
          
          unit_weight = atof(num);
        }
        if(general_weight > -.01 && general_weight < .01) 
        {
          scale_drift = general_weight;
        }
        else if((general_weight - scale_drift) >= unit_weight-.002 && (general_weight - scale_drift) <= unit_weight+.003)
        { // check if data read is within tolerance
          Serial.print(message);
            digitalWrite(GOOD_PIN, HIGH);
            IS_GOOD_SET = true;
            delay(100);
            digitalWrite(GOOD_PIN, LOW);;
        } 
        else 
        {
            digitalWrite(BAD_PIN, HIGH);
            IS_BAD_SET = true;
            delay(75);
            digitalWrite(BAD_PIN, LOW);;
        }
        //Reset for the next message
         message_pos = 0;
      }
  }
    Serial.flush();
}
